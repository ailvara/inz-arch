package pl.sylwiamikolajczuk.homenav;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.opencv.core.Mat;

import android.util.Log;

/**
 * Klasa realizuj�ca ci�g�o�� poszukiwania lokalizacji
 * @author Sylwia Miko�ajczuk
 *
 */
public class BackgroundScan
{
	private static String LOGTAG = "BackgroundScan";
	
	private MainActivity activity;
	private BlockingQueue<Runnable> queue;
	private ThreadPoolExecutor pool;
		
	/**
	 * Konstruktor klasy
	 * @param activity - aktywno�� wywo�uj�ca
	 */
	BackgroundScan(MainActivity activity)
	{
		int cores = Runtime.getRuntime().availableProcessors();
		Log.v("", "run cores: "+cores);
		this.activity = activity;
		
	    queue = new LinkedBlockingQueue<Runnable>();
	    pool = new ThreadPoolExecutor(
                Math.max(cores-1,1),
                Math.max(cores-1,1),
                0,
                TimeUnit.NANOSECONDS,
                queue);
	}
	
	/**
	 * Decyzja o dodaniu nowego zadania do puli w�tk�w
	 * @param frame - aktualna ramka
	 */
    public void run(Mat frame) 
    {
    	Log.v(LOGTAG, "Pool run: "+pool.getActiveCount()+" "+pool.getQueue().size()+" "+pool.getCompletedTaskCount());

		if(frame!=null && pool.getQueue().size()<2) {
	       	pool.execute(new Localize(frame));
	    }
	    else Log.v(LOGTAG, "can't add new thread");
    }

    /**
     * W�tek realizuj�cy wykrycie lokalizacji
     * @author Sylwia Miko�ajczuk
     *
     */
	private class Localize implements Runnable {

		private Mat frame;
		
		/**
		 * Konstruktor klasy Scan
		 * @param frame - aktualna ramka
		 */
		Localize(Mat frame)
		{
			Log.v(LOGTAG, "thread start");
			this.frame = frame.clone();
		}
		
		/**
		 * Sprawd� lokalizacj� na podstawie aktualnej ramki
		 */
		@Override
		public void run() 
		{
			long start = System.currentTimeMillis();
			
			final int[] pos = activity.nav.currentPosition(frame);

			if(pos != null) {
				activity.runOnUiThread(new Runnable() {
				     @Override
				     public void run() 
				     {
				    	 activity.floorPlan.drawPosition(pos[0],pos[1],true);
				    	 Log.v(LOGTAG, "match found!");
				     }
				});
			}
			else Log.v(LOGTAG, "position is unknown");
			Log.v(LOGTAG, "exec time: "+(System.currentTimeMillis()-start));
		}
	}
}