package pl.sylwiamikolajczuk.homenav;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import android.content.Context;
import android.util.Log;

/**
 * Klasa realizuj�ca poszukiwanie �cie�ki pomi�dzy pokojami
 * @author Sylwia Miko�ajczuk
 *
 */
public class PathFinder {
	
	private static String LOGTAG = "Graph";

	ArrayList<Door> doors;
	ArrayList<Room> rooms;
	ArrayList<Edge> edges;
	
	/**
	 * Konstruktor klasy PathFinder
	 * @param context
	 */
	PathFinder(Context context)
	{
		doors = new ArrayList<Door>();
		rooms = new ArrayList<Room>();
		edges = new ArrayList<Edge>();
		
		DataHandler config = new DataHandler(context, Statics.FLOOR+"_mapconf");
		for(String name : config.getAvailableLocations())
		{
			int[] loc;
			//Log.v(LOGTAG, name+" "+name.contains("door")+" "+name.contains("room"));
			if(name.contains("door")) {
				loc = config.getLocation(name);
				doors.add(new Door(loc, name));
			}
			else if(name.contains("room")) {
				loc = config.getSquare(name);
				rooms.add(new Room(loc, name));
			}
		}
		
		for(Room room : rooms){
			for(Door door : doors){
				if(areNeighbours(room, door)) {
					Log.v(LOGTAG, room.name+" "+door.name+" are neighbours");
					edges.add(new Edge(room, door));
				}
			}
		}

		for(Room room : rooms){
			int routesCount = rooms.size();
			HashMap<Room, ArrayList<Door>> routes = room.getNearestRoutes(new ArrayList<Door>());
			
			Set<Room> lastFoundRooms = routes.keySet();
			for(Room r : lastFoundRooms) Log.v(LOGTAG, "found rooms: "+r.name);
			
			while(routes.size() < routesCount)
			{
				HashMap<Room, ArrayList<Door>> farRoutes = new HashMap<Room, ArrayList<Door>>();
				for(Room nextRoom : lastFoundRooms)
				{
					ArrayList<Door> routeSoFar = routes.get(nextRoom);
					HashMap<Room, ArrayList<Door>> roomRoutes = nextRoom.getNearestRoutes(routeSoFar);
					farRoutes.putAll(roomRoutes);
				}
				if(farRoutes.isEmpty()) break;
				Iterator<Entry<Room, ArrayList<Door>>> iterator = farRoutes.entrySet().iterator();
				while(iterator.hasNext())
				{
					Entry<Room, ArrayList<Door>> item = iterator.next();
					if(routes.containsKey(item.getKey())) continue;
					else routes.put(item.getKey(), item.getValue());
				}
				lastFoundRooms = farRoutes.keySet();
			}
			
			room.routes = routes;
		}
	}
	
	/**
	 * Pobierz �cie�k� z punktu do punktu
	 * @param start - punkt pocz�tkowy
	 * @param end - punkt ko�cowy
	 * @return
	 */
	public ArrayList<int[]> getRoute(int[] start, int[] end)
	{
		Room startRoom = getRoom(start);
		Room endRoom = getRoom(end);
		
		ArrayList<int[]> response = new ArrayList<int[]>();
		
		response.add(start);
		if(startRoom == null || endRoom == null) Log.v(LOGTAG, "No room found");
		else if(startRoom == endRoom) Log.v(LOGTAG, "Same room");
		else response.addAll(startRoom.getRoute(endRoom));
		response.add(end);
		
		return response;
	}
	
	/**
	 * Sprawd�, czy drzwi nale�� do pokoju
	 * @param room - badany pok�j
	 * @param door - badane drzwi
	 * @return - czy le�� obok siebie
	 */
	private boolean areNeighbours(Room room, Door door)
	{
		int threshold = 5;
		
		Log.v(LOGTAG, room.name+", "+door.name+": "+room.getX()+" "+door.getX()+" "+room.getX2());
		
		if(
			(
				(Math.abs(door.getX() - room.getX()) < threshold) 
				||
				(Math.abs(door.getX() - room.getX2()) < threshold) 
			)
			&& 
			(
				(door.getY() > room.getY()) 
				&& 
				(door.getY() < room.getY2())
			)
		) return true;

		Log.v(LOGTAG, room.name+", "+door.name+": "+room.getY()+" "+door.getY()+" "+room.getY2());
		
		if(
			(
				(Math.abs(door.getY() - room.getY()) < threshold) 
				||
				(Math.abs(door.getY() - room.getY2()) < threshold) 
			)
			&& 
			(
				(door.getX() > room.getX()) 
				&& 
				(door.getX() < room.getX2())
			)
		) return true;
		
		return false;
	}
	
	/**
	 * Sprawd�, w kt�rym pokoju le�y punkt
	 * @param point - wybrany punkt
	 * @return pok�j zawierj�cy
	 */
	private Room getRoom(int[] point)
	{
		for(Room room : rooms)
		{
			Log.v(LOGTAG, room.getX()+" "+point[0]+" "+room.getX2());
			Log.v(LOGTAG, room.getY()+" "+point[1]+" "+room.getY2());
			if(
				( point[0] > room.getX() && point[0] < room.getX2() ) 
				&&
				( point[1] > room.getY() && point[1] < room.getY2() )
			) return room;
		}
		Log.v(LOGTAG, "No room found");
		return null;
	}
	
	/**
	 * Klasa reprezentuj�ca punkt pocz�tkowy lub ko�cowy trasy
	 * @author Sylwia Miko�ajczuk
	 *
	 */
	private class Node
	{
		int[] xy;
		String name;
		
		Node(int[] xy, String name)
		{
			this.xy = xy;
			this.name = name;
		}
		
		int getX()
		{ 
			return xy[0];
		}
		
		int getY()
		{
			return xy[1];
		}
	}
	
	/**
	 * Punkt w postaci pokoju
	 * @author Sylwia Miko�ajczuk
	 *
	 */
	private class Room extends Node
	{
		HashMap<Room, ArrayList<Door>> routes;
		
		Room(int[] xy, String name) {
			super(xy, name);
		}
		
		int getX2()
		{
			return xy[2];
		}
		
		int getY2()
		{
			return xy[3];
		}
		
		/**
		 * Pobierz list� drzwi wychodz�cych z pokoju
		 * @return lista drzwi
		 */
		ArrayList<Door> getDoors()
		{
			ArrayList<Door> result = new ArrayList<Door>();
			for(Edge edge : edges) {
				if(edge.room.equals(this)) {
					result.add(edge.door);
					Log.v(LOGTAG, "Found door: "+edge.room.name+" "+edge.door.name);
				}
			}
			return result;
		}
		
		/**
		 * Pobierz tras� z tego pokoju do innego
		 * @param endRoom - do jakiego pokoju poprowadzi�
		 * @return lista punkt�w po�rednich
		 */
		ArrayList<int[]> getRoute(Node endRoom)
		{
			ArrayList<int[]> response = new ArrayList<int[]>();
			ArrayList<Door> doors = routes.get(endRoom);
			for(Door door : doors){
				int[] loc = {door.getX(), door.getY()};
				response.add(loc);
			}
			return response;
		}
		
		/**
		 * Lista pokoj�w s�siednich wraz ze �cie�k� (drzwiami po�rednimi)
		 * @param routeSoFar - trasa do do��czenia na pocz�tku
		 * @return mapa pokoj�w na �cie�ki
		 */
		HashMap<Room, ArrayList<Door>> getNearestRoutes(ArrayList<Door> routeSoFar)
		{
			HashMap<Room, ArrayList<Door>> routes = new HashMap<Room, ArrayList<Door>>();
			ArrayList<Door> myDoors = this.getDoors();
			
			for(Door door : myDoors){
				Room endRoom = door.getSecondRoom(this);
				ArrayList<Door> route = new ArrayList<Door>();
				route.addAll(routeSoFar);
				route.add(door);
				routes.put(endRoom, route);
			}
			return routes;
		}
	}
	
	/**
	 * Punkt w postaci drzwi
	 * @author Sylwia Miko�ajczuk
	 *
	 */
	private class Door extends Node
	{
		Door(int[] xy, String name)
		{
			super(xy, name);
		}
		
		/**
		 * Pobierz pokoje s�siednie
		 * @return pokoje, kt�re ��cz� drzwi
		 */
		ArrayList<Room> getRooms()
		{
			ArrayList<Room> result = new ArrayList<Room>();
			for(Edge edge : edges) if(edge.door.equals(this)) result.add(edge.room);
			return result;
		}
		
		/**
		 * Pobierz pok�j, z kt�rym ��cz� te drzwi
		 * @param firstRoom - pok�j, kt�rego s�siada szukamy
		 * @return pok�j po drugiej stronie drzwi
		 */
		Room getSecondRoom(Room firstRoom)
		{
			for(Room room : getRooms()){
				if(room != firstRoom) return room;
			}
			return null;
		}
	}
	
	/**
	 * Po��czenia pokoj�w i drzwi
	 * @author Sylwia Miko�ajczuk
	 *
	 */
	private class Edge
	{
		Room room;
		Door door;
		
		Edge(Room room, Door door)
		{
			this.room = room;
			this.door = door;
		}
	}
}
