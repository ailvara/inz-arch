package pl.sylwiamikolajczuk.homenav;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.drawable.PictureDrawable;
import android.util.Log;
import android.view.View;

/**
 * Klasa odpowiedzialna za wy�wietlenie mapki i wybranych punkt�w
 * @author Sylwia Miko�ajczuk
 *
 */
public class FloorPlan extends View 
{
	private static String LOGTAG = "FloorPlan";
	
	private Paint bgPaint;
	private Paint pointPaint;
	private Paint pathPaint;
	private Bitmap plan;
	
	private boolean drawPosition = false;
	private boolean drawDestination = false;
	private int prevX;
	private int prevY;
	private int posX;
	private int posY;
	private int desX;
	private int desY;
	
	private PathFinder pathFinder;
	
	/**
	 * Konstruktor klasy FloorPlan
	 * @param context
	 */
    public FloorPlan(Context context) 
    {
        super(context);
        
        SVG svg = null;
		try {
			File file = new File(Statics.FOLDER+Statics.FLOOR+".svg");
			FileInputStream in = new FileInputStream(file);
			svg = SVGParser.getSVGFromInputStream(in);
		} catch (IOException e) {
			Log.v(LOGTAG, e.getMessage());
		}
		
		if(svg == null){
			try {
				svg = SVGParser.getSVGFromAsset(context.getAssets(), "demo.svg");
			} catch (IOException e) {
				Log.v(LOGTAG, e.getMessage());
				e.printStackTrace();
			}
		}
		
        Picture picture = svg.getPicture();
        plan = toBitmap(picture);
        
        bgPaint = new Paint();
        pointPaint = new Paint();
        pointPaint.setColor(Color.parseColor("#ea0000"));
        pointPaint.setTextSize(20); 
        pointPaint.setShadowLayer(2, 1, 1, Color.BLACK);
        pointPaint.setStrokeWidth(2);
        pathPaint = new Paint();
        pathPaint.setColor(Color.parseColor("#613cff"));
        pathPaint.setTextSize(20); 
        pathPaint.setShadowLayer(2, 1, 1, Color.BLACK);
        pathPaint.setStrokeWidth(5);
        
        pathFinder = new PathFinder(context);
    }

    @Override
    protected void onDraw(Canvas canvas) 
    {
        super.onDraw(canvas);
        canvas.drawBitmap(plan, 0, 0, bgPaint);
        
        if(drawPosition) {
        	canvas.drawCircle(posX, posY, 10, pointPaint);
        	canvas.drawText("Tu jeste�", posX+15, posY+15, pointPaint); 
        }
        if(drawDestination) {
        	canvas.drawCircle(desX, desY, 10, pathPaint);
        	canvas.drawText("Cel", desX+15, desY+15, pathPaint); 
        }
        if(drawPosition && drawDestination) drawPath(canvas);
    }
    
    /**
     * Ustaw punkt pocz�tkowy
     * @param x - wsp�rz�dna x
     * @param y - wsp�rz�dna y
     * @param draw - czy rysowa� punkt?
     */
    public void drawPosition(int x, int y, boolean draw)
    {
    	if(draw) drawPosition = true;
    	else drawPosition = false;
    	posX = x;
    	posY = y;
    	if(prevX != posX || prevY != posY) {
    		this.invalidate();
    		prevX = posX;
    		prevY = posY;
    	}
    	
    }
    
    /**
     * Ustaw punkt docelowy
     * @param x - wsp�rz�dna x
     * @param y - wsp�rz�dna y
     * @param draw - czy rysowa�?
     */
    public void drawDestination(int x, int y, boolean draw)
    {
    	if(draw) drawDestination = true;
    	else drawDestination = false;
    	desX = x;
    	desY = y;
    	this.invalidate();
    }
    
    /**
     * Wy�wietl po��czenie pomi�dzy punktem pocz�tkowym i docelowym
     * @param canvas
     */
	private void drawPath(Canvas canvas)
	{
		int[] startPoint = {posX, posY};
		int[] endPoint   = {desX, desY};
		
		ArrayList<int[]> points = pathFinder.getRoute(startPoint, endPoint);
		for(int[] point: points)
		{
			canvas.drawLine(startPoint[0], startPoint[1], point[0], point[1], pathPaint);
			startPoint = point;
		}
    }
	
	/**
	 * Konwertuje wczytany plan, aby m�g� zosta� wy�wietlony
	 * @param plan - wczytana mapka
	 * @return Bitmapa do wy�wietlenia
	 */
	private Bitmap toBitmap(Picture plan)
    {
        PictureDrawable pd = new PictureDrawable(plan);
        Bitmap bitmap = Bitmap.createBitmap(pd.getIntrinsicWidth(),pd.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawPicture(pd.getPicture());
        return bitmap;
    }
}
