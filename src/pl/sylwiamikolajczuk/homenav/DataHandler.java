package pl.sylwiamikolajczuk.homenav;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

/**
 * Klasa realizuj�ca interakcj� z plikami json
 * @author Sylwia Miko�ajczuk
 * 
 */
public class DataHandler {
	
	private static String LOGTAG = "DataHandler";
	
	private JsonObject doc;
	private JsonParser parser;
	private File file;
		
	/**
	 * Konstruktor klasy DataHandler
	 * @param context
	 * @param path - �cie�ka dostepu do pliku
	 */
	DataHandler(Context context, String name)
	{
		try {
			String path = Statics.FOLDER+name+".json";
			file = new File(path);
			if(!file.exists()) {
				Log.v(LOGTAG, "File doesn't exist!");
				FileOutputStream out = new FileOutputStream(file);
				out.write("{}".getBytes());
				out.close();
			}
			FileInputStream in = new FileInputStream(file);
			byte[] buffer = new byte[in.available()];
	        in.read(buffer);
	        in.close();
	        String json = new String(buffer, "UTF-8");
	        parser = new JsonParser();
	        doc = parser.parse(json).getAsJsonObject();
		} catch (IOException e) {
			Log.v(LOGTAG, e.getMessage());
		} catch(JsonParseException e){
			Log.v(LOGTAG, e.getMessage());
		}
	}
	
	/**
	 * Pobierz obiekt zapisany w pliku json
	 * @param name - nazwa obiektu
	 * @return obiekt
	 */
	public JsonObject getJson(String name)
	{
		return doc.get(name).getAsJsonObject();
	}
	
	/**
	 * Pobierz wsp�rz�dne zapisanego punktu
	 * @param elementName - nazwa punktu
	 * @return wsp�rz�dne x i y
	 */
	public int[] getLocation(String elementName)
	{
		JsonObject object = getJson(elementName);
		int[] pos = new int[2];
		pos[0] = object.get("x").getAsInt();
		pos[1] = object.get("y").getAsInt();
		return pos;
	}
	
	public int[] getSquare(String elementName)
	{
		JsonObject object = getJson(elementName);
		int[] pos = new int[4];
		pos[0] = object.get("x").getAsInt();
		pos[1] = object.get("y").getAsInt();
		pos[2] = object.get("x2").getAsInt();
		pos[3] = object.get("y2").getAsInt();
		Log.v(LOGTAG, "success");
		return pos;
	}
	
	/**
	 * Dodaj nowy punkt
	 * @param name - nazwa punktu
	 * @param x - wsp�rz�dna x
	 * @param y - wsp�rz�dna y
	 */
	public void addLocation(String name, int x, int y)
	{
		JsonObject object = new JsonObject();
		object.addProperty("name", name);
		object.addProperty("x", x);
		object.addProperty("y", y);
		doc.add(name, object);
		Gson gson = new Gson();
        String json = gson.toJson(doc);
		try {
			FileOutputStream out = new FileOutputStream(file);
			out.write(json.getBytes());
			out.close();
			Log.v(LOGTAG, file.getAbsolutePath());
		} catch (FileNotFoundException e) {
			Log.v(LOGTAG, e.getMessage());
		} catch (IOException e) {
			Log.v(LOGTAG, e.getMessage());
		}
	}
	
	/**
	 * Dodaj obiekt do pliku
	 * @param object - obiekt do dodania
	 */
	public void addObject(JsonObject object)
	{
		doc.add(object.get("name").getAsString(), object);
		Gson gson = new Gson();
        String json = gson.toJson(doc);
		try {
			FileOutputStream out = new FileOutputStream(file);
			out.write(json.getBytes());
			out.close();
			Log.v(LOGTAG, file.getAbsolutePath());
		} catch (FileNotFoundException e) {
			Log.v(LOGTAG, e.getMessage());
		} catch (IOException e) {
			Log.v(LOGTAG, e.getMessage());
		}
	}
	
	/**
	 * Pobierz list� nazw dodanych lokalizacji
	 * @return lista nazw obiekt�w zapisanych w pliku
	 */
	public ArrayList<String> getAvailableLocations()
	{
		ArrayList<String> availableLocations = new ArrayList<String>();
		Set<Entry<String, JsonElement>> entrySet = doc.entrySet();
		for(Entry<String, JsonElement> entry : entrySet)
		{
			availableLocations.add(entry.getKey());
		}
		return availableLocations; 
	}
}
