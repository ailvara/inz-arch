package pl.sylwiamikolajczuk.homenav;

import org.opencv.core.Mat;

import android.util.Base64;

import com.google.gson.JsonObject;

/**
 * Klasa reprezentuj�ca punkt odniesienia
 * @author Sylwia Miko�ajczuk
 *
 */
public class ReferencePoint
{
	private String name;
	private Mat descriptors;
	private int[] coordinates;
	
	/**
	 * Konstruktor klasy ReferencePoint
	 * @param name - nazwa punktu
	 * @param descriptors - deskryptory punktu
	 * @param coordinates - wsp�rz�dne punktu
	 */
	ReferencePoint(String name, Mat descriptors, int[] coordinates)
	{
		this.name = name;
		this.descriptors = descriptors;
		this.coordinates = coordinates;
	}
	
	/**
	 * Konstruktor klasy ReferencePoint
	 * @param point - punkt w formie obiektu Json
	 */
	ReferencePoint(JsonObject point)
	{
		this.name = point.get("name").getAsString();
		coordinates = new int[2];
		coordinates[0] = point.get("x").getAsInt();
		coordinates[1] = point.get("y").getAsInt();

		//na podstawie http://answers.opencv.org/question/8873/best-way-to-store-a-mat-object-in-android/
		String descriptorsString = point.get("descriptors").getAsString();       
		byte[] descriptorsBinary = Base64.decode(descriptorsString.getBytes(), Base64.DEFAULT); 
		descriptors = new Mat(point.get("rows").getAsInt(), point.get("cols").getAsInt(), point.get("type").getAsInt());
		descriptors.put(0, 0, descriptorsBinary);
	}
		
	/**
	 * Serializacja obiektu
	 * @return obiekt w formie json
	 */
	public JsonObject toJson()
	{
		JsonObject point = new JsonObject();
		point.addProperty("name", name);
		point.addProperty("x", coordinates[0]);
		point.addProperty("y", coordinates[1]);

		//na podstawie http://answers.opencv.org/question/8873/best-way-to-store-a-mat-object-in-android/
		int cols = descriptors.cols();
        int rows = descriptors.rows();
        byte[] descriptorsToBinary = new byte[cols*rows*(int) descriptors.elemSize()];
        descriptors.get(0, 0, descriptorsToBinary);
        String descriptorsString = new String(Base64.encode(descriptorsToBinary, Base64.DEFAULT));
		point.addProperty("descriptors", descriptorsString);
		point.addProperty("cols", cols);
		point.addProperty("rows", rows);
		point.addProperty("type", descriptors.type());
		
		return point;
	}
	
	/**
	 * Sprawd�, czy obiekt jest poprawny
	 * @return decyzja (prawda lub fa�sz)
	 */
	public boolean validate()
	{
		if (descriptors == null) return false;
		if (coordinates == null) return false;
		else return true;
	}
	
	/**
	 * Pobierz nazw� punktu
	 * @return nazwa obiektu
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Pobierz wsp�rz�dne punktu
	 * @return wsp�rz�dne x i y
	 */
	public int[] getCoordinates()
	{
		return coordinates;
	}
	
	/**
	 * Pobierz deskryptory punktu
	 * @return deskryptory obiektu
	 */
	public Mat getDescriptors()
	{
		return descriptors;
	}
}
