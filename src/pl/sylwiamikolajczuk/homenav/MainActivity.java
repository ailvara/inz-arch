package pl.sylwiamikolajczuk.homenav;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * G��wna aktywno�� aplikacji
 * @author Sylwia Miko�ajczuk
 *
 */
public class MainActivity extends Activity implements CvCameraViewListener2 
{
	private static String LOGTAG = "MainActivity";
	
	private CameraBridgeViewBase camera;
	private Mat currentFrame;
	private Context context;
	private BackgroundScan scan;
	private MainActivityHelper helper;
	private long start;
	
	ObjectDetection nav;
	FloorPlan floorPlan;
	
	/**
	 * Za�adowanie OpenCV
	 */
	private BaseLoaderCallback loaderCallback = new BaseLoaderCallback(this) 
	{
	    @Override
	    public void onManagerConnected(int status) {
	        switch (status) {
	            case LoaderCallbackInterface.SUCCESS:
	            {
	        		if(nav == null) initNav();
	                camera.enableView();
	                Log.v(LOGTAG, "Starttime: "+(System.currentTimeMillis()-start));
	            } break;
	            default:
	            {
	                super.onManagerConnected(status);
	            } break;
	        }
	    }
	};
	
	/**
	 * Za�adowanie obiekt�w klasy
	 */
	private void initNav()
	{
		nav = new ObjectDetection(context);
		helper = new MainActivityHelper(this, nav);
		if(Statics.FLOOR == null) initFloor();
		scan = new BackgroundScan(this);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    start = System.currentTimeMillis();
	    context = this.getApplicationContext();
	    if(Statics.FLOOR == null) initFloor();
		
	    setContentView(R.layout.activity_main);
	    
	    camera = (CameraBridgeViewBase)findViewById(R.id.HelloOpenCvView);
	    camera.setCvCameraViewListener(this);
	    
	    RelativeLayout plan = (RelativeLayout)findViewById(R.id.plan);
	    floorPlan = new FloorPlan(context);
	    plan.addView(floorPlan);
	}

	/**
	 * Stw�rz nowy kontekst
	 * @param name - nazwa nowego kontekstu
	 */
	private void createNewFloor(String name)
	{
		String empty = "{}";
		File dir = new File(Statics.ROOT+name);
		dir.mkdirs();
		File points = new File(dir, name+".json");
		File dests = new File(dir, name+"_destinations.json");
		File svg = new File(dir, name+".svg");
		File mapconf = new File(dir, name+"_mapconf.json");
		
		try {
			String demo ="";
			//http://stackoverflow.com/questions/9544737/read-file-from-assets
			BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open("demo.svg")));
			String line = reader.readLine();
		    while (line != null) {
		    	demo += line;
		    	line = reader.readLine(); 
		    }
		    
		    String demoConf ="";
			//http://stackoverflow.com/questions/9544737/read-file-from-assets
			reader = new BufferedReader(new InputStreamReader(getAssets().open("demo_mapconf.json" +
					"")));
			line = reader.readLine();
		    while (line != null) {
		    	demoConf += line;
		    	line = reader.readLine(); 
		    }
			
			FileOutputStream out1 = new FileOutputStream(points);
			out1.write(empty.getBytes());
			FileOutputStream out2 = new FileOutputStream(dests);
			out2.write(empty.getBytes());
			FileOutputStream out3 = new FileOutputStream(svg);
			out3.write(demo.getBytes());
			FileOutputStream out4 = new FileOutputStream(mapconf);
			out4.write(demoConf.getBytes());
			out1.close();
			out2.close();
			out3.close();
			out4.close();
		} catch (FileNotFoundException e) {
			Log.v(LOGTAG, e.getMessage());
		} catch (IOException e) {
			Log.v(LOGTAG, e.getMessage());
		}
	}

	/**
	 * Inicjalizacja kontekstu
	 */
	private void initFloor()
	{
		SharedPreferences sp = getSharedPreferences("home_data", Activity.MODE_PRIVATE);
		Statics.setFloor(sp.getString("last_floor", null));
		if(Statics.FLOOR == null) {
			Statics.setFloor("default");
			File dir = new File(Environment.getExternalStorageDirectory()+"/homenav/");
			dir.mkdirs();
			createNewFloor("default");
		    SharedPreferences.Editor editor = sp.edit();
			editor.putString("last_floor", "default");
			editor.commit();
		}
		else {
			File dir = new File(Statics.FOLDER);
			if(!dir.exists()){
				dir.mkdirs();
				createNewFloor(Statics.FLOOR);
			}
		}
		
	    Log.v("", ""+Statics.FLOOR);
	}

	/**
	 * Metoda przycisku "prowad� do"
	 * @param view - przycisk wywo�uj�cy
	 */
	public void guideTo(View view)
	{
		helper.guideTo(floorPlan);
	}
	
	/**
	 * Metoda przycisku "Dodaj punkt odniesienia"
	 * @param view - przycisk wywo�uj�cy
	 */
	public void addPoint(View view)
	{
		helper.addPoint(currentFrame);
	}
	
	/**
	 * Metoda przycisku "Dodaj punkt docelowy"
	 * @param view - przycisk wywo�uj�cy
	 */
	public void addDestination(View view)
	{
		helper.addDestination();
	}
	
	/**
	 * Poka� instrukcj� obs�ugi
	 * @param view - przycisk wywo�uj�cy
	 */
	public void showManual(View view)
	{
		helper.showManual();
	}

	/**
	 * Metoda przycisku "Zmie� plan budynku"
	 * @param view - przycisk wywo�uj�cy
	 */
	public void changeFloor(View view)
	{
		final ArrayList<String> availableFloors = new ArrayList<String>();
		File homeDir = new File(Environment.getExternalStorageDirectory()+"/homenav");
		for (File f : homeDir.listFiles()) {
			Log.v("LOGTAG", f.getName());
		    if (f.isDirectory()) availableFloors.add(f.getName());
		}
		
		LinearLayout ll = new LinearLayout(context);
		ll.setOrientation(LinearLayout.VERTICAL);
				
		final Spinner chooseFloor = new Spinner(context);
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner, availableFloors);
		chooseFloor.setAdapter(adapter);
		chooseFloor.setSelection(adapter.getPosition(Statics.FLOOR));
		ll.addView(chooseFloor);

		final EditText getName = new EditText(context);
		getName.clearFocus();
		ll.addView(getName);
		
		Button addNewFloor = new Button(context);
		addNewFloor.setText(R.string.add_new);
		addNewFloor.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				String name = getName.getText().toString();
				if(name == null || name.length() < 1) 
					Toast.makeText(context, R.string.no_place_name, Toast.LENGTH_SHORT).show();
				else if(availableFloors.contains(name))
					Toast.makeText(context, R.string.already_exists, Toast.LENGTH_SHORT).show();
				else {
					createNewFloor(name);
					availableFloors.add(name);
					chooseFloor.setSelection(adapter.getPosition(name));
				}
			}
		});
		ll.addView(addNewFloor);
		
		new AlertDialog.Builder(this)
		.setTitle(R.string.choose_floor)
		.setView(ll)
		.setNegativeButton(R.string.discard, null)
		.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				Statics.setFloor(chooseFloor.getSelectedItem().toString());
				RelativeLayout plan = (RelativeLayout)findViewById(R.id.plan);
				plan.removeView(floorPlan);
			    floorPlan = new FloorPlan(context);
			    plan.addView(floorPlan);
			    SharedPreferences sp = getSharedPreferences("home_data", Activity.MODE_PRIVATE);
				SharedPreferences.Editor editor = sp.edit();
				editor.putString("last_floor", Statics.FLOOR);
				editor.commit();
			    initNav();
			}
		})
		.show();		
	}
	
	@Override
	public void onPause()
	{
	    super.onPause();
	    if (camera != null) camera.disableView();
	}

	public void onDestroy() 
	{
	    super.onDestroy();
	    if (camera != null) camera.disableView();
	}

	/**
	 * Pobranie klatki z kamery
	 */
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) 
	{
		currentFrame = inputFrame.gray();
		if(scan!=null ) scan.run(currentFrame);
	    return inputFrame.rgba();
	}

	@Override
	public void onResume()
	{
	    super.onResume();
	    OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this, loaderCallback);
	}

	@Override
	public void onCameraViewStarted(int width, int height) 
	{
	}

	@Override
	public void onCameraViewStopped() 
	{
	}
}
