package pl.sylwiamikolajczuk.homenav;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.drawable.PictureDrawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Klasa odpowiedzialna za pobranie wybranych wsp�rz�dnych od u�ytkownika
 * @author Sylwia Miko�ajczuk
 *
 */
public class MiniFloorPlan extends View 
{
	private static String LOGTAG = "MiniFloorPlan";
	
	private Paint bgPaint, selectionPaint;
	private Bitmap plan;
	private boolean drawSelection = false;
	private int selX, selY;
	
    public MiniFloorPlan(Context context) 
    {
        super(context);
        
        SVG svg = null;
		try {
			File file = new File(Statics.FOLDER+Statics.FLOOR+".svg");
			FileInputStream in = new FileInputStream(file);
			svg = SVGParser.getSVGFromInputStream(in);
		} catch (IOException e){
			Log.v(LOGTAG, e.getMessage());
		}
		
		if(svg == null){
			try {
				svg = SVGParser.getSVGFromAsset(context.getAssets(), "demo.svg");
			} catch (IOException e) {
				Log.v(LOGTAG, e.getMessage());
			}
		}

        Picture picture = svg.getPicture();
        plan = toBitmap(picture);
        
        bgPaint = new Paint();
        selectionPaint = new Paint();
        selectionPaint.setColor(Color.GREEN);
        
        this.setOnTouchListener(new OnTouchListener()
        {
			@Override
			public boolean onTouch(View bitmap, MotionEvent me) {
				selX = (int) me.getX();
				selY = (int) me.getY();
				Log.v(LOGTAG, "Selx: "+selX+", sely: "+selY);
				if(selY > plan.getHeight() || selX > plan.getWidth())
					drawSelection = false;
				else
					drawSelection = true;
				invalidate();
				return true;
			}
        });
    }

    @Override
    protected void onDraw(Canvas canvas) 
    {
        super.onDraw(canvas);
        canvas.drawBitmap(plan, 0, 0, bgPaint);
        if(drawSelection) canvas.drawCircle(selX, selY, 10, selectionPaint);
    }
    
	/**
	 * Pobierz wsp�rz�dne ustalonego punktu
	 * @return wsp�rz�dne x i y lub null, je�li nie ustalono
	 */
	public int[] getLocation()
	{
		if(drawSelection == false) return null;
		int[] result = new int[2];
		result[0] = selX;
		result[1] = selY;
		return result;
	}

    /**
	 * Konwertuje wczytany plan, aby m�g� zosta� wy�wietlony
	 * @param plan - wczytana mapka
	 * @return Bitmapa do wy�wietlenia
	 */
	private Bitmap toBitmap(Picture picture)
    {
        PictureDrawable pd = new PictureDrawable(picture);
        Bitmap bitmap = Bitmap.createBitmap(pd.getIntrinsicWidth(),pd.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawPicture(pd.getPicture());
        return bitmap;
    }
}
