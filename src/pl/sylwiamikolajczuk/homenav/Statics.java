package pl.sylwiamikolajczuk.homenav;

import android.os.Environment;

/**
 * Klasa dostarczająca innym klasom wspólne dla wszystkich wartości
 * @author Sylwia Mikołajczuk
 *
 */
public class Statics 
{
	public static String FLOOR = null;
	public static String FILENAME = "sample";
	public static String ROOT = Environment.getExternalStorageDirectory()+"/homenav/";
	public static String FOLDER = ROOT+FLOOR+"/";
	
	
	public static Object SYNC = new Object();
	
	public static void setFloor(String name)
	{
		FLOOR = name;
		FOLDER = ROOT+FLOOR+"/";
	}
	
	/*
	 * References
	 * http://stackoverflow.com/questions/19945411/android-java-how-can-i-parse-a-local-json-file-from-assets-folder-into-a-listvi
	 * http://stackoverflow.com/questions/17898480/object-detection-with-opencv-feature-matching-with-a-threshold-similarity-score
	 * http://stackoverflow.com/questions/10273646/android-how-to-convert-picture-from-webview-capturepicture-to-byte-and-bac
	 * 
	 */
}