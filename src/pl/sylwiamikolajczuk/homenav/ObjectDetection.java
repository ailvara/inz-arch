package pl.sylwiamikolajczuk.homenav;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.highgui.Highgui;

import com.google.gson.JsonObject;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

/**
 * Klasa zarz�dzaj�ca punktami odniesienia i realizuj�ca ich dopasowywanie
 * @author Sylwia Miko�ajczuk
 *
 */
public class ObjectDetection {

	private static int MATCHED_POINTS = 10;
	private static float REJECT_THRESHOLD = 300f;
	private static String LOGTAG = "ObjectDetection";

	private ArrayList<ReferencePoint> referencePoints;
	private DataHandler pointsBase;
	
	ObjectDetection(Context context)
	{
		referencePoints = new ArrayList<ReferencePoint>();
        pointsBase = new DataHandler(context, Statics.FLOOR);
		ArrayList<String> points = pointsBase.getAvailableLocations();
		
		for(String name : points)
		{
			JsonObject data = pointsBase.getJson(name);
			ReferencePoint point = new ReferencePoint(data);
			referencePoints.add(point);
		}
	}
	
	/**
	 * Zapisz punkt odniesienia
	 * @param name - nazwa punktu
	 * @param frame - obraz punktu
	 * @param coordinates - wsp�rz�dne punktu
	 * @return sukces czy pora�ka (zbyt ma�o punkt�w)
	 */
	public boolean savePoint(String name, Mat frame, int[] coordinates)
	{
		Mat descr = descriptors(frame);
		if(descr == null) return false;
		ReferencePoint point = new ReferencePoint(name, descr, coordinates);
		referencePoints.add(point);
		pointsBase.addObject(point.toJson());
		Highgui.imwrite(Environment.getExternalStorageDirectory().getAbsolutePath()
				+"/"+Statics.FOLDER+"/"+name+".jpg", frame);
		return true;
	}
	
	/**
	 * Okre�� aktualn� pozycj�
	 * @param frame - aktualny widok z kamery
	 * @return wsp�rz�dne po�o�enia
	 */
	public int[] currentPosition(Mat frame)
	{
		frame.convertTo(frame, CvType.CV_8U);
		
		Mat frameDescriptors = descriptors(frame);
		if(frameDescriptors == null) return null;
		HashMap<Float,int[]> matches = new HashMap<Float,int[]>();
		float bestMatch = REJECT_THRESHOLD;
		
		synchronized(Statics.SYNC)
		{
			for(ReferencePoint item : referencePoints)
			{
				float match = match(frameDescriptors, item.getDescriptors());
				matches.put(match, item.getCoordinates());
				Log.v(LOGTAG, "item name: "+item.getName()+" match: "+match);
				bestMatch = Math.min(bestMatch, match);
			}
		}
		Log.v(LOGTAG, "bestMatch: "+bestMatch);
		if(bestMatch == REJECT_THRESHOLD) return null;
		else return matches.get(bestMatch);
	}
	
	/**
	 * Obliczenie deskryptor�w obrazu
	 * @param image - obraz, kt�rego deskryptory mamy obliczy�
	 * @return deskryptory obrazu
	 */
	public Mat descriptors(Mat image)
	{
		FeatureDetector featureDetector = FeatureDetector.create(FeatureDetector.ORB); 
        MatOfKeyPoint keyPoints = new MatOfKeyPoint();
        featureDetector.detect(image, keyPoints);
        if(keyPoints.size().height < MATCHED_POINTS) return null;
        Mat descriptors = new Mat();
        DescriptorExtractor descriptorExtractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
        descriptorExtractor.compute(image, keyPoints, descriptors);
        return descriptors;
	}
	
	/**
	 * Oblicz warto�� dopasowania deskryptor�w ramki i zapisanego punktu odniesienia
	 * @param frameDesriptors - deskryptory aktualnej ramki
	 * @param objectDescriptors - deskryptory obrazu punktu
	 * @return warto�� dopasowania
	 */
	private float match(Mat frameDesriptors, Mat objectDescriptors)
	{
		MatOfDMatch matches = new MatOfDMatch();
        DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMINGLUT);
        matcher.match(frameDesriptors, objectDescriptors, matches);
        List<DMatch> matchesList = matches.toList();

        float distancesSum = 0f;
        ArrayList<Float> distances = new ArrayList<Float>();
        for(DMatch match : matchesList) distances.add(match.distance);
        Collections.sort(distances);
        
        for(int i = 0; i<MATCHED_POINTS; i++) distancesSum += distances.get(i);
        return distancesSum;
	}
}
