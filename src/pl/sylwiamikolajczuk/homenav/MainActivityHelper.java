package pl.sylwiamikolajczuk.homenav;

import java.util.ArrayList;

import org.opencv.core.Mat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Klasa realizuj�ca niekt�re metody przycisk�w z klasy MainActivity (w celu wi�kszej przejrzysto�ci)
 * @author Sylwia Miko�ajczuk
 * 
 */
public class MainActivityHelper {

	private MainActivity main;
	private Context context;
	private ObjectDetection nav;
	private DataHandler destinations;
	
	/**
	 * Konstruktor klasy MainActivityHelper
	 * @param main - klasa, kt�r� wspomaga
	 * @param nav - obiekt detekcji lokalizacji
	 */
	MainActivityHelper(MainActivity main, ObjectDetection nav)
	{
		this.main = main;
		this.context = main.getApplicationContext();
    	this.nav = nav;
    	destinations = new DataHandler(context, Statics.FLOOR+"_destinations");
	}
	
	/**
	 * Prowad� do
	 * @param floorPlan - plan do wyrysowania punktu
	 */
	public void guideTo(final FloorPlan floorPlan)
	{
		ArrayList<String> availableDestinations = destinations.getAvailableLocations();
		if(availableDestinations.size() == 0) Toast.makeText(
				context,
				R.string.no_destinations_exist, 
				Toast.LENGTH_LONG
				).show();
		else {
			final Spinner chooseDestination = new Spinner(context);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
	                R.layout.spinner, availableDestinations);
			chooseDestination.setAdapter(adapter);
			
			//http://www.androidsnippets.com/prompt-user-input-with-an-alertdialog
			new AlertDialog.Builder(main)
				.setTitle(R.string.choose_destination)
				.setView(chooseDestination)
				.setNegativeButton(R.string.discard, null)
				.setPositiveButton(R.string.guide_me, new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int whichButton) 
					{
						String desName = chooseDestination.getSelectedItem().toString();
						int[] destination = destinations.getLocation(desName);
						if(destination == null) Toast.makeText(context, R.string.error, Toast.LENGTH_SHORT).show();
						else floorPlan.drawDestination(destination[0],destination[1],true);
					}
				})
				.show();
		}
	}
	
	/**
	 * Dodaj punkt odniesienia
	 * @param frame - widok punktu
	 */
	public void addPoint(final Mat frame)
	{
		final MiniFloorPlan plan = new MiniFloorPlan(context);
		
		final AlertDialog alert = new AlertDialog.Builder(main)
		.setTitle(R.string.add_point)
		.setView(plan)
		.setNegativeButton(R.string.discard, null)
		.setPositiveButton(R.string.add, null)
		.create();
		alert.setOnShowListener(new DialogInterface.OnShowListener() {
		    @Override
		    public void onShow(DialogInterface dialog) {
		        Button b = alert.getButton(AlertDialog.BUTTON_POSITIVE);
		        b.setOnClickListener(new View.OnClickListener() {
		            @Override
		            public void onClick(View view) {
						int[] coordinates = plan.getLocation();
						if(coordinates == null) Toast.makeText(context, R.string.no_coordinates, Toast.LENGTH_SHORT).show();
						else {
							addPoint(coordinates, frame);
			                alert.dismiss();
						}
		            }
		        });
		    }
		});
		alert.show();
	}
	
	/**
	 * Po ustaleniu parametr�w przeprowadza w�a�ciwe dodanie punktu
	 * @param coordinates - wsp�rz�dne punktu
	 * @param image - obraz punktu
	 */
	private void addPoint(int[] coordinates, Mat image)
	{
		SharedPreferences sp = main.getSharedPreferences("home_data", Activity.MODE_PRIVATE);
		int pointsNo = sp.getInt(Statics.FLOOR+"pointsNo", 0);
		pointsNo++;
		
		String name = Statics.FILENAME+pointsNo;
		boolean success;
		synchronized(Statics.SYNC){
			success = nav.savePoint(name, image, coordinates);
		}

		if(!success) Toast.makeText(context, R.string.few_points, Toast.LENGTH_SHORT).show();
		else Toast.makeText(context, R.string.success, Toast.LENGTH_SHORT).show();
		
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(Statics.FLOOR+"pointsNo", pointsNo);
		editor.commit();
	}
	
	/**
	 * Dodaj punkt docelowy
	 */
	public void addDestination()
	{
		LinearLayout ll = new LinearLayout(context);
		ll.setOrientation(LinearLayout.VERTICAL);
		TextView getNameLabel = new TextView(context);
		getNameLabel.setText(R.string.name_label);
		ll.addView(getNameLabel);
		final EditText getName = new EditText(context);
		ll.addView(getName);
		final MiniFloorPlan plan = new MiniFloorPlan(context);
		ll.addView(plan);
		
		final AlertDialog alert = new AlertDialog.Builder(main)
		.setTitle(R.string.choose_destination)
		.setView(ll)
		.setNegativeButton(R.string.discard, null)
		.setPositiveButton(R.string.add, null)
		.create();
		alert.setOnShowListener(new DialogInterface.OnShowListener() {
		    @Override
		    public void onShow(DialogInterface dialog) {
		        Button b = alert.getButton(AlertDialog.BUTTON_POSITIVE);
		        b.setOnClickListener(new View.OnClickListener() {
		            @Override
		            public void onClick(View view) {String name = getName.getText().toString();
						int[] coordinates = plan.getLocation();
						if(name == null || name.length() < 1) 
							Toast.makeText(context, R.string.no_place_name, Toast.LENGTH_SHORT).show();
						else if(destinations.getAvailableLocations().contains(name)) 
							Toast.makeText(context, R.string.already_exists, Toast.LENGTH_SHORT).show();
						else if(coordinates == null) 
							Toast.makeText(context, R.string.no_coordinates, Toast.LENGTH_SHORT).show();
						else {
							destinations.addLocation(name, coordinates[0], coordinates[1]);
							Toast.makeText(context,R.string.success, Toast.LENGTH_SHORT).show();
							alert.dismiss();
						}
		            }
		        });
		    }
		});
		alert.show();
	}
	
	public void showManual()
	{
		TextView manual = new TextView(context);
		manual.setTextSize(19);
		manual.setPadding(10, 10, 10, 10);
		manual.setText(R.string.manual);
		manual.setMovementMethod(new ScrollingMovementMethod());
		new AlertDialog.Builder(main)
		.setTitle(R.string.show_manual)
		.setView(manual)
		.setPositiveButton(R.string.understood, null)
		.show();
	}
}
